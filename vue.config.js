/*
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})
*/
const path = require('path');
function resolve(dir) {
  return path.join(__dirname, dir)
}

// El archivo de configuración principal del proyecto
module.exports = {
  // configuración de webpack para una modificación más precisa https://cli.vuejs.org/zh/config/#chainwebpack
  chainWebpack: (config) => {
    // Modifica el archivo para introducir una ruta personalizada
    config.resolve.alias
      .set('@', resolve('src'))
      .set('style', resolve('src/assets/style'))
  },
  
}